package io.chirp.messenger;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

public class Getter extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_getter);
        Intent intent = getIntent();
        String key = intent.getStringExtra("key");
        JSONObject jsonObj = null;
        TextView top =findViewById(R.id.top);
        TextView bot=findViewById(R.id.down);
        try {
            jsonObj = new JSONObject(key);
        String date =jsonObj.get("date").toString();
        String shopName=jsonObj.get("shop_name").toString();
        String totalSum=jsonObj.get("total_sum").toString();
        String cassToken =jsonObj.get("cass_token").toString();
        top.setText(date+"\n"+shopName+"\n"+totalSum+"\n"+cassToken);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
